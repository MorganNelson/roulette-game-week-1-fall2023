import java.util.Scanner;
public class Roulette{
    private int money;
    private Scanner input;
    private RouletteWheel game;
    
    public Roulette(){
        this.input = new Scanner(System.in);
        this.money = 1000;
        this.game = new RouletteWheel();
    }
    public void startGame(){
        int turn=1;
        boolean choice=false;
        do{
        System.out.println("Turn: "+ turn);
        choice=doYouBet();
        if(choice){
        int amountBet = betAmount(choice);
        calculateMoney(amountBet,spinTheWheel());
        turn++;
        }else{
                System.out.println("Ok then, thanks for playing!");
            }
        }while(choice);
    }

    public boolean doYouBet(){
        System.out.println("You have: " + this.money);
        System.out.println("Do you want to bet? (Yes/No) (Case Insensitive)");
        boolean betOrNot= false;
        String response="";
        //I was thinking about putting a complicated check but then I realised it wouldn't give me more points :(
        do{
            response= input.next();
            //code used from previous assignments
            if(!(response.equalsIgnoreCase("Yes")) && !(response.equalsIgnoreCase("No"))){
                System.out.println("Try again, input is not from two choices given");
                }

        }while(!(response.equalsIgnoreCase("Yes")) && !(response.equalsIgnoreCase("No")));
        
        
        if(response.equalsIgnoreCase("Yes")){
            betOrNot= true;
        }
        return betOrNot;
    }

    public int betAmount(boolean betOrNot){
        int amtBet =0;
        if(betOrNot){
            System.out.println("You have: " + this.money);
            System.out.println("How much would you like to bet?");
            do{
               input.nextLine(); 
               amtBet = input.nextInt(); 
            }while(amtBet>this.money);
            return amtBet;
        }else{
            System.out.println("Well, have a nice day!");
            return amtBet;
        }
    }

    public boolean spinTheWheel(){
        int nbBet=0;
        System.out.println("On which number would you like to bet between 0 and 37?");
        do{
               input.nextLine(); 
               nbBet = input.nextInt();
        }while(nbBet>=37 || nbBet<0);
        game.spin();
        System.out.println("Spinning...");
        System.out.println("Spinning...");
        System.out.println("Spinning...");

        System.out.println("Your number is...");
        System.out.println(game.getValue()+"!!!");
        
        if(game.getValue()==nbBet){
            System.out.println("YOU WON!");
            return true;
        }else{
            System.out.println("You lost..."); 
            return false;
        }
    }

    public void calculateMoney(int amtBet,boolean wonOrNot){
        //couldn't find a better name
        int schrodingersMoney=0;
        if(wonOrNot){
            schrodingersMoney = amtBet*35;
            System.out.println("You gained:" +schrodingersMoney);
            this.money += schrodingersMoney;
            System.out.println("You now have :" + this.money);
        }else{
            schrodingersMoney-=amtBet;
            System.out.println("You lost: " + amtBet);
            this.money -= amtBet;
            System.out.println("You now have: " + this.money);
        }
        System.out.println("That's it, thanks for playing!");
    }
    

    public static void main (String args[]){
        Roulette rouletteGame = new Roulette();
        rouletteGame.startGame();
    }
}